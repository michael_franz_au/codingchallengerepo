using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace VendingModel
{
    public enum VendingResult
    {
        None = 0,
        AddedMoney = 1,
        AddMoneyFailed = 2,
        Refund = 3,
        RefundFailed = 4,
        InsufficientFunds = 5,
        NoProductsInLane = 6,
        ProductsAvailableInLane = 7,
        ProductsVendSuccess = 8,
    }
    public class VendingProduct
    {
        public VendingProduct()
        {

        }

        public int Key { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool isPurchased { get; set; }
    }
    public class OperationResult
    {
        public OperationResult()
        {

        }
        public VendingResult vending_result { get; set; }

        public decimal refund { get; set; }

        public decimal balance { get; set; }

        public string display_message { get; set; }

        /// <summary>
        /// A Selected or vended product
        /// </summary>
        public VendingProduct product { get; set; }

        /// <summary>
        /// Unsold products
        /// </summary>
        public List<VendingProduct> products { get; set; }
    }

    public class VendingMoney
    {
        public VendingMoney()
        {
            transaction_id = new Guid().ToString();
        }

        public decimal amount { get; set; }

        public string transaction_id { get; set; }

    }
    public class VendingCache : IVendingCache
    {

        public VendingCache()
        {
            Transactions = new List<VendingMoney>();
            Products = new List<VendingProduct>();
        }
        public List<VendingMoney> Transactions { get; set; }

        public List<VendingProduct> Products { get; set; }

        public OperationResult AddProduct(VendingProduct product)
        {
            var result = new OperationResult();

            try
            {
                Products.Add(product);
                result.display_message = $"{product.Name} added";
      
                return result;
            }
            catch
            {
                result.display_message = "AddProduct failed";
                return result;
            }
        }
        public OperationResult Purchase(VendingProduct product)
        {
            var result = new OperationResult();

            try
            {

                if (product.Price > Balance())
                {
                    result.display_message = "Insufficent funds";
                    result.vending_result = VendingResult.InsufficientFunds;
                    result.balance = Balance();
                    result.products = ProductsRemaining();
                    return result;
                }
                if (Products.Where(x => !x.isPurchased && x.Key == product.Key).Count() == 0)
                {
                    result.display_message = "No more products";
                    result.vending_result = VendingResult.NoProductsInLane;
                    result.balance = Balance();
                    result.products = ProductsRemaining();
                    return result;
                }


                VendingMoney money = new VendingMoney();
                money.amount = product.Price * -1m;
                Transactions.Add(money);
                var found = Products.FirstOrDefault(x => !x.isPurchased && x.Key == product.Key);
                found.isPurchased = true;
                result.product =  found;
                result.vending_result = VendingResult.ProductsVendSuccess;
                result.products = ProductsRemaining();
                result.balance = Balance();

                return result;
            }
            catch
            {
                result.display_message = "Purchase failed";
                return result;
            }
        }
        public OperationResult EmptyProducts()
        {
            var result = new OperationResult();

            Products = new List<VendingProduct>();

            result.products = Products;
            result.balance = Balance();

            return result;

        }
        public OperationResult EmptyTransactions()
        {
            var result = new OperationResult();

            Transactions = new List<VendingMoney>();

            result.products = Products;
            result.balance = 0;

            return result;

        }
        public OperationResult CacheContents()
        {
            var result = new OperationResult();

            Transactions = new List<VendingMoney>();
            Products = new List<VendingProduct>();

            result.products = ProductsRemaining();
            result.balance = Balance();
            return result;

        }
        public OperationResult AddMoney(VendingMoney money)
        {
            var result = new OperationResult();

            try
            {
                Transactions.Add(money);
                result.display_message = "Money added";
                result.balance = Balance();
                result.products = Products;
                result.vending_result = VendingResult.AddedMoney;
                return result;
            }
            catch
            {
                result.display_message = "Money failed to add";
                result.vending_result = VendingResult.AddMoneyFailed;
                result.products = Products;
                return result;
            }
        }

        public OperationResult Refund()
        {
            var result = new OperationResult();

            try
            {
                result.display_message = "Money refunded";
                result.refund = Balance();
                result.balance = 0;
                result.vending_result = VendingResult.Refund;
                result.products = ProductsRemaining();
                Transactions.Clear();

                result.balance = Balance();
               
                return result;
            }
            catch
            {
                result.display_message = "Refund Failed";
                return result;
            }
        }
        public OperationResult ProductQuery(int key)
        {
            var result = new OperationResult();

            try
            {
                result.product = Products.FirstOrDefault(x => x.Key == key);
                result.products = ProductsRemaining();
                result.balance = Balance();
                result.vending_result = VendingResult.ProductsAvailableInLane;
                if (result.product == null) result.vending_result = VendingResult.NoProductsInLane;
                return result;
            }
            catch
            {
                result.display_message = "Product query failed";
                return result;
            }
        }

        private decimal Balance()
        {
            return Transactions.Sum(item => item.amount);
        }
        private List<VendingProduct> ProductsRemaining()
        {
            return Products.Where(x => x.isPurchased == false).ToList();
        }
    }

    public interface IVendingCache
    {
        OperationResult AddMoney(VendingMoney money);

        OperationResult AddProduct(VendingProduct product);

        OperationResult Purchase(VendingProduct product);

        OperationResult Refund();

        OperationResult ProductQuery(int key);

        OperationResult CacheContents();

        OperationResult EmptyProducts();
        OperationResult EmptyTransactions();

    }
}
