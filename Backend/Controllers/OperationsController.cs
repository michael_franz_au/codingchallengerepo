﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VendingModel;

namespace VendingBackend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OperationsController : ControllerBase
    {
        private readonly IVendingCache _cache;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;


        public OperationsController(IVendingCache cache, IConfiguration config, IWebHostEnvironment hostingEnvironment)
        {
            _cache = cache;
            _configuration = config;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Route("[action]")]
        public string IsOperational()
        {
            return DateTime.Now.ToString();
        }


        [HttpPost]
        [Route("[action]")]
        public OperationResult AddMoney([FromBody]VendingMoney money)
        {
            return _cache.AddMoney(money);
        }

        [HttpGet]
        [Route("[action]")]
        public OperationResult Refund()
        {
            return _cache.Refund();
        }
        
        [HttpGet]
        [Route("[action]/{key}")]
        public OperationResult QueryProduct(int key)
        {
            return _cache.ProductQuery(key);
        }

        [HttpPut]
        [Route("[action]")]
        public OperationResult Purchase(VendingProduct product)
        {
            return _cache.Purchase(product);
        }

        [HttpDelete]
        [Route("[action]")]
        public OperationResult EmptyMachine()
        {
            return _cache.EmptyProducts();
        }

        [HttpPut]
        [Route("[action]")]
        public OperationResult FillMachine()
        {
            _cache.EmptyProducts();
            string path =  _hostingEnvironment.ContentRootPath + _configuration["ProductsPath"];
            if (System.IO.File.Exists(path))
            {
                var products = JsonSerializer.Deserialize<List<VendingProduct>>(System.IO.File.ReadAllText(path), new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                foreach (var product in products)
                {
                    _cache.AddProduct(product);
                }
            }
            return _cache.ProductQuery(0);
        }

        [HttpGet]
        [Route("[action]")]
        public OperationResult QueryStock()
        {
            return _cache.ProductQuery(0);
        }

        [HttpGet]
        [Route("[action]")]
        public OperationResult CacheContents()
        {
            return _cache.CacheContents();
        }
    }
}
