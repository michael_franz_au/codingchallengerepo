﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VendingModel;

namespace VendingBackend.Controllers
{

    public class TestController : Controller
    {
        private readonly IVendingCache _cache;

        public TestController(IVendingCache reserve)
        {
            _cache = reserve;
        }

        [HttpGet]
        public string Serialize()
        {
            List<VendingProduct> products = new List<VendingProduct>()
            {
                new VendingProduct() { Key = 11,  Name = "Mars bar", Price = 2.50m, isPurchased =false },
                new VendingProduct() { Key = 22,  Name = "600ml Coke Zero", Price = 5.50m , isPurchased =false},
                new VendingProduct() { Key = 33,  Name = "Samboy Chips 200gm", Price = 4.50m, isPurchased =false},
            };

            return JsonSerializer.Serialize<List<VendingProduct>>(products);
        }

        [HttpGet]
        public OperationResult AddMoney()
        {
            VendingMoney money = new VendingMoney();
            money.amount = 5m;
            return _cache.AddMoney(money);
        }

        [HttpGet]
        public OperationResult Refund()
        {
            return _cache.Refund();
        }

        [HttpGet]
        public OperationResult Purchase()
        {
            VendingProduct product = new VendingProduct() { Key = 11 };

            return _cache.Purchase(product);
        }
    }
}
