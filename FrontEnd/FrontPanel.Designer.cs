﻿namespace VendingMachine
{
    partial class FrontPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnTwoDollar = new System.Windows.Forms.Button();
            this.bntTwentyCents = new System.Windows.Forms.Button();
            this.bntTenCents = new System.Windows.Forms.Button();
            this.bntOneDollar = new System.Windows.Forms.Button();
            this.bntFiveCents = new System.Windows.Forms.Button();
            this.btnFiftyCents = new System.Windows.Forms.Button();
            this.Money = new System.Windows.Forms.GroupBox();
            this.bntRefund = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.key_1 = new System.Windows.Forms.Button();
            this.key_2 = new System.Windows.Forms.Button();
            this.key_3 = new System.Windows.Forms.Button();
            this.key_4 = new System.Windows.Forms.Button();
            this.key_5 = new System.Windows.Forms.Button();
            this.key_6 = new System.Windows.Forms.Button();
            this.key_7 = new System.Windows.Forms.Button();
            this.key_8 = new System.Windows.Forms.Button();
            this.key_9 = new System.Windows.Forms.Button();
            this.btnVend = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.bntClear = new System.Windows.Forms.Button();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Payment = new System.Windows.Forms.GroupBox();
            this.key_0 = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAdmin = new System.Windows.Forms.TextBox();
            this.bntFillProducts = new System.Windows.Forms.Button();
            this.btnEmptyProducts = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bntEmptyChute = new System.Windows.Forms.Button();
            this.dgvVendingChute = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMoneyReturn = new System.Windows.Forms.TextBox();
            this.bsChute = new System.Windows.Forms.BindingSource(this.components);
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.bntRefundedMoney = new System.Windows.Forms.Button();
            this.Money.SuspendLayout();
            this.Payment.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendingChute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsChute)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTwoDollar
            // 
            this.btnTwoDollar.Location = new System.Drawing.Point(28, 107);
            this.btnTwoDollar.Name = "btnTwoDollar";
            this.btnTwoDollar.Size = new System.Drawing.Size(75, 47);
            this.btnTwoDollar.TabIndex = 0;
            this.btnTwoDollar.Text = "$2.00";
            this.btnTwoDollar.UseVisualStyleBackColor = true;
            this.btnTwoDollar.Click += new System.EventHandler(this.btnTwoDollar_Click);
            // 
            // bntTwentyCents
            // 
            this.bntTwentyCents.Location = new System.Drawing.Point(28, 170);
            this.bntTwentyCents.Name = "bntTwentyCents";
            this.bntTwentyCents.Size = new System.Drawing.Size(75, 47);
            this.bntTwentyCents.TabIndex = 1;
            this.bntTwentyCents.Text = "0.20c";
            this.bntTwentyCents.UseVisualStyleBackColor = true;
            this.bntTwentyCents.Click += new System.EventHandler(this.bntTwentyCents_Click);
            // 
            // bntTenCents
            // 
            this.bntTenCents.Location = new System.Drawing.Point(123, 170);
            this.bntTenCents.Name = "bntTenCents";
            this.bntTenCents.Size = new System.Drawing.Size(75, 47);
            this.bntTenCents.TabIndex = 2;
            this.bntTenCents.Text = "0.10c";
            this.bntTenCents.UseVisualStyleBackColor = true;
            this.bntTenCents.Click += new System.EventHandler(this.bntTenCents_Click);
            // 
            // bntOneDollar
            // 
            this.bntOneDollar.Location = new System.Drawing.Point(123, 107);
            this.bntOneDollar.Name = "bntOneDollar";
            this.bntOneDollar.Size = new System.Drawing.Size(75, 47);
            this.bntOneDollar.TabIndex = 3;
            this.bntOneDollar.Text = "$1.00";
            this.bntOneDollar.UseVisualStyleBackColor = true;
            this.bntOneDollar.Click += new System.EventHandler(this.bntOneDollar_Click);
            // 
            // bntFiveCents
            // 
            this.bntFiveCents.Location = new System.Drawing.Point(220, 170);
            this.bntFiveCents.Name = "bntFiveCents";
            this.bntFiveCents.Size = new System.Drawing.Size(75, 47);
            this.bntFiveCents.TabIndex = 4;
            this.bntFiveCents.Text = "0.05c";
            this.bntFiveCents.UseVisualStyleBackColor = true;
            this.bntFiveCents.Click += new System.EventHandler(this.bntFiveCents_Click);
            // 
            // btnFiftyCents
            // 
            this.btnFiftyCents.Location = new System.Drawing.Point(220, 107);
            this.btnFiftyCents.Name = "btnFiftyCents";
            this.btnFiftyCents.Size = new System.Drawing.Size(75, 47);
            this.btnFiftyCents.TabIndex = 5;
            this.btnFiftyCents.Text = "0.50c";
            this.btnFiftyCents.UseVisualStyleBackColor = true;
            this.btnFiftyCents.Click += new System.EventHandler(this.btnFiftyCents_Click);
            // 
            // Money
            // 
            this.Money.Controls.Add(this.bntRefund);
            this.Money.Controls.Add(this.btnFiftyCents);
            this.Money.Controls.Add(this.bntOneDollar);
            this.Money.Controls.Add(this.bntFiveCents);
            this.Money.Controls.Add(this.bntTenCents);
            this.Money.Controls.Add(this.label1);
            this.Money.Controls.Add(this.btnTwoDollar);
            this.Money.Controls.Add(this.bntTwentyCents);
            this.Money.Controls.Add(this.txtBalance);
            this.Money.Location = new System.Drawing.Point(12, 37);
            this.Money.Name = "Money";
            this.Money.Size = new System.Drawing.Size(349, 312);
            this.Money.TabIndex = 7;
            this.Money.TabStop = false;
            this.Money.Text = "Payment";
            // 
            // bntRefund
            // 
            this.bntRefund.Location = new System.Drawing.Point(220, 247);
            this.bntRefund.Name = "bntRefund";
            this.bntRefund.Size = new System.Drawing.Size(75, 47);
            this.bntRefund.TabIndex = 6;
            this.bntRefund.Text = "Refund";
            this.bntRefund.UseVisualStyleBackColor = true;
            this.bntRefund.Click += new System.EventHandler(this.bntRefund_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Balance";
            // 
            // txtBalance
            // 
            this.txtBalance.Location = new System.Drawing.Point(28, 63);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(87, 20);
            this.txtBalance.TabIndex = 17;
            this.txtBalance.TextChanged += new System.EventHandler(this.txtBalance_TextChanged);
            // 
            // key_1
            // 
            this.key_1.Location = new System.Drawing.Point(43, 113);
            this.key_1.Name = "key_1";
            this.key_1.Size = new System.Drawing.Size(45, 45);
            this.key_1.TabIndex = 8;
            this.key_1.Text = "1";
            this.key_1.UseVisualStyleBackColor = true;
            this.key_1.Click += new System.EventHandler(this.key_1_Click);
            // 
            // key_2
            // 
            this.key_2.Location = new System.Drawing.Point(109, 113);
            this.key_2.Name = "key_2";
            this.key_2.Size = new System.Drawing.Size(45, 45);
            this.key_2.TabIndex = 9;
            this.key_2.Text = "2";
            this.key_2.UseVisualStyleBackColor = true;
            this.key_2.Click += new System.EventHandler(this.key_2_Click);
            // 
            // key_3
            // 
            this.key_3.Location = new System.Drawing.Point(178, 113);
            this.key_3.Name = "key_3";
            this.key_3.Size = new System.Drawing.Size(45, 45);
            this.key_3.TabIndex = 10;
            this.key_3.Text = "3";
            this.key_3.UseVisualStyleBackColor = true;
            this.key_3.Click += new System.EventHandler(this.key_3_Click);
            // 
            // key_4
            // 
            this.key_4.Location = new System.Drawing.Point(43, 164);
            this.key_4.Name = "key_4";
            this.key_4.Size = new System.Drawing.Size(45, 45);
            this.key_4.TabIndex = 11;
            this.key_4.Text = "4";
            this.key_4.UseVisualStyleBackColor = true;
            this.key_4.Click += new System.EventHandler(this.key_4_Click);
            // 
            // key_5
            // 
            this.key_5.Location = new System.Drawing.Point(109, 164);
            this.key_5.Name = "key_5";
            this.key_5.Size = new System.Drawing.Size(45, 45);
            this.key_5.TabIndex = 12;
            this.key_5.Text = "5";
            this.key_5.UseVisualStyleBackColor = true;
            this.key_5.Click += new System.EventHandler(this.key_5_Click);
            // 
            // key_6
            // 
            this.key_6.Location = new System.Drawing.Point(178, 164);
            this.key_6.Name = "key_6";
            this.key_6.Size = new System.Drawing.Size(45, 45);
            this.key_6.TabIndex = 13;
            this.key_6.Text = "6";
            this.key_6.UseVisualStyleBackColor = true;
            this.key_6.Click += new System.EventHandler(this.key_6_Click);
            // 
            // key_7
            // 
            this.key_7.Location = new System.Drawing.Point(43, 215);
            this.key_7.Name = "key_7";
            this.key_7.Size = new System.Drawing.Size(45, 45);
            this.key_7.TabIndex = 14;
            this.key_7.Text = "7";
            this.key_7.UseVisualStyleBackColor = true;
            this.key_7.Click += new System.EventHandler(this.key_7_Click);
            // 
            // key_8
            // 
            this.key_8.Location = new System.Drawing.Point(109, 215);
            this.key_8.Name = "key_8";
            this.key_8.Size = new System.Drawing.Size(45, 45);
            this.key_8.TabIndex = 15;
            this.key_8.Text = "8";
            this.key_8.UseVisualStyleBackColor = true;
            this.key_8.Click += new System.EventHandler(this.key_8_Click);
            // 
            // key_9
            // 
            this.key_9.Location = new System.Drawing.Point(178, 215);
            this.key_9.Name = "key_9";
            this.key_9.Size = new System.Drawing.Size(45, 45);
            this.key_9.TabIndex = 16;
            this.key_9.Text = "9";
            this.key_9.UseVisualStyleBackColor = true;
            this.key_9.Click += new System.EventHandler(this.key_9_Click);
            // 
            // btnVend
            // 
            this.btnVend.Location = new System.Drawing.Point(178, 274);
            this.btnVend.Name = "btnVend";
            this.btnVend.Size = new System.Drawing.Size(45, 43);
            this.btnVend.TabIndex = 18;
            this.btnVend.Text = "Vend";
            this.btnVend.UseVisualStyleBackColor = true;
            this.btnVend.Click += new System.EventHandler(this.btnVend_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Product";
            // 
            // txtProduct
            // 
            this.txtProduct.Location = new System.Drawing.Point(43, 56);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.ReadOnly = true;
            this.txtProduct.Size = new System.Drawing.Size(100, 20);
            this.txtProduct.TabIndex = 20;
            this.txtProduct.TextChanged += new System.EventHandler(this.txtProduct_TextChanged);
            // 
            // bntClear
            // 
            this.bntClear.Location = new System.Drawing.Point(43, 272);
            this.bntClear.Name = "bntClear";
            this.bntClear.Size = new System.Drawing.Size(45, 45);
            this.bntClear.TabIndex = 22;
            this.bntClear.Text = "Clear";
            this.bntClear.UseVisualStyleBackColor = true;
            this.bntClear.Click += new System.EventHandler(this.bntClear_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(149, 56);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            this.txtPrice.Size = new System.Drawing.Size(74, 20);
            this.txtPrice.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Price";
            // 
            // Payment
            // 
            this.Payment.Controls.Add(this.txtMessage);
            this.Payment.Controls.Add(this.key_0);
            this.Payment.Controls.Add(this.txtID);
            this.Payment.Controls.Add(this.btnVend);
            this.Payment.Controls.Add(this.label3);
            this.Payment.Controls.Add(this.key_1);
            this.Payment.Controls.Add(this.txtPrice);
            this.Payment.Controls.Add(this.key_2);
            this.Payment.Controls.Add(this.bntClear);
            this.Payment.Controls.Add(this.key_3);
            this.Payment.Controls.Add(this.label2);
            this.Payment.Controls.Add(this.key_4);
            this.Payment.Controls.Add(this.txtProduct);
            this.Payment.Controls.Add(this.key_5);
            this.Payment.Controls.Add(this.key_6);
            this.Payment.Controls.Add(this.key_9);
            this.Payment.Controls.Add(this.key_7);
            this.Payment.Controls.Add(this.key_8);
            this.Payment.Location = new System.Drawing.Point(385, 37);
            this.Payment.Name = "Payment";
            this.Payment.Size = new System.Drawing.Size(249, 378);
            this.Payment.TabIndex = 25;
            this.Payment.TabStop = false;
            this.Payment.Text = "Product Selection";
            // 
            // key_0
            // 
            this.key_0.Location = new System.Drawing.Point(109, 272);
            this.key_0.Name = "key_0";
            this.key_0.Size = new System.Drawing.Size(45, 45);
            this.key_0.TabIndex = 26;
            this.key_0.Text = "0";
            this.key_0.UseVisualStyleBackColor = true;
            this.key_0.Click += new System.EventHandler(this.key_0_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(43, 87);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(45, 20);
            this.txtID.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAdmin);
            this.groupBox1.Controls.Add(this.bntFillProducts);
            this.groupBox1.Controls.Add(this.btnEmptyProducts);
            this.groupBox1.Location = new System.Drawing.Point(12, 383);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 139);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Products Admin";
            // 
            // txtAdmin
            // 
            this.txtAdmin.Location = new System.Drawing.Point(28, 104);
            this.txtAdmin.Name = "txtAdmin";
            this.txtAdmin.ReadOnly = true;
            this.txtAdmin.Size = new System.Drawing.Size(183, 20);
            this.txtAdmin.TabIndex = 23;
            // 
            // bntFillProducts
            // 
            this.bntFillProducts.Location = new System.Drawing.Point(136, 34);
            this.bntFillProducts.Name = "bntFillProducts";
            this.bntFillProducts.Size = new System.Drawing.Size(75, 47);
            this.bntFillProducts.TabIndex = 21;
            this.bntFillProducts.Text = "Fill";
            this.bntFillProducts.UseVisualStyleBackColor = true;
            this.bntFillProducts.Click += new System.EventHandler(this.bntFill_Click);
            // 
            // btnEmptyProducts
            // 
            this.btnEmptyProducts.Location = new System.Drawing.Point(28, 34);
            this.btnEmptyProducts.Name = "btnEmptyProducts";
            this.btnEmptyProducts.Size = new System.Drawing.Size(75, 47);
            this.btnEmptyProducts.TabIndex = 20;
            this.btnEmptyProducts.Text = "Empty";
            this.btnEmptyProducts.UseVisualStyleBackColor = true;
            this.btnEmptyProducts.Click += new System.EventHandler(this.btnEmpty_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bntRefundedMoney);
            this.groupBox2.Controls.Add(this.bntEmptyChute);
            this.groupBox2.Controls.Add(this.dgvVendingChute);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtMoneyReturn);
            this.groupBox2.Location = new System.Drawing.Point(664, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 245);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Out";
            // 
            // bntEmptyChute
            // 
            this.bntEmptyChute.Location = new System.Drawing.Point(210, 20);
            this.bntEmptyChute.Name = "bntEmptyChute";
            this.bntEmptyChute.Size = new System.Drawing.Size(33, 23);
            this.bntEmptyChute.TabIndex = 24;
            this.bntEmptyChute.Text = "X";
            this.bntEmptyChute.UseVisualStyleBackColor = true;
            this.bntEmptyChute.Click += new System.EventHandler(this.bntEmptyChute_Click);
            // 
            // dgvVendingChute
            // 
            this.dgvVendingChute.AllowUserToAddRows = false;
            this.dgvVendingChute.AllowUserToDeleteRows = false;
            this.dgvVendingChute.AllowUserToResizeColumns = false;
            this.dgvVendingChute.AllowUserToResizeRows = false;
            this.dgvVendingChute.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVendingChute.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVendingChute.ColumnHeadersVisible = false;
            this.dgvVendingChute.Location = new System.Drawing.Point(9, 56);
            this.dgvVendingChute.Name = "dgvVendingChute";
            this.dgvVendingChute.ReadOnly = true;
            this.dgvVendingChute.RowHeadersVisible = false;
            this.dgvVendingChute.Size = new System.Drawing.Size(234, 114);
            this.dgvVendingChute.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Money Return";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Product Chute";
            // 
            // txtMoneyReturn
            // 
            this.txtMoneyReturn.Location = new System.Drawing.Point(99, 201);
            this.txtMoneyReturn.Name = "txtMoneyReturn";
            this.txtMoneyReturn.ReadOnly = true;
            this.txtMoneyReturn.Size = new System.Drawing.Size(100, 20);
            this.txtMoneyReturn.TabIndex = 21;
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(43, 346);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ReadOnly = true;
            this.txtMessage.Size = new System.Drawing.Size(180, 20);
            this.txtMessage.TabIndex = 20;
            // 
            // bntRefundedMoney
            // 
            this.bntRefundedMoney.Location = new System.Drawing.Point(210, 199);
            this.bntRefundedMoney.Name = "bntRefundedMoney";
            this.bntRefundedMoney.Size = new System.Drawing.Size(33, 23);
            this.bntRefundedMoney.TabIndex = 25;
            this.bntRefundedMoney.Text = "X";
            this.bntRefundedMoney.UseVisualStyleBackColor = true;
            this.bntRefundedMoney.Click += new System.EventHandler(this.btnRefundedMoney_Click);
            // 
            // FrontPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 554);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Payment);
            this.Controls.Add(this.Money);
            this.Name = "FrontPanel";
            this.Text = "Vending Machine";
            this.Money.ResumeLayout(false);
            this.Money.PerformLayout();
            this.Payment.ResumeLayout(false);
            this.Payment.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendingChute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsChute)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTwoDollar;
        private System.Windows.Forms.Button bntTwentyCents;
        private System.Windows.Forms.Button bntTenCents;
        private System.Windows.Forms.Button bntOneDollar;
        private System.Windows.Forms.Button bntFiveCents;
        private System.Windows.Forms.Button btnFiftyCents;
        private System.Windows.Forms.GroupBox Money;
        private System.Windows.Forms.Button bntRefund;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Button key_1;
        private System.Windows.Forms.Button key_2;
        private System.Windows.Forms.Button key_3;
        private System.Windows.Forms.Button key_4;
        private System.Windows.Forms.Button key_5;
        private System.Windows.Forms.Button key_6;
        private System.Windows.Forms.Button key_7;
        private System.Windows.Forms.Button key_8;
        private System.Windows.Forms.Button key_9;
        private System.Windows.Forms.Button btnVend;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProduct;
        private System.Windows.Forms.Button bntClear;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox Payment;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bntFillProducts;
        private System.Windows.Forms.Button btnEmptyProducts;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button key_0;
        private System.Windows.Forms.TextBox txtAdmin;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMoneyReturn;
        private System.Windows.Forms.Button bntEmptyChute;
        private System.Windows.Forms.DataGridView dgvVendingChute;
        private System.Windows.Forms.BindingSource bsChute;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Button bntRefundedMoney;
    }
}

