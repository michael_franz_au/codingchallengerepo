﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public static class Extensions
    {
        public static T Coalesce<T>(this T value, T isNotnull)
        {
            if (value != null) return (T)value;
            return (T)isNotnull;
        }
    }
}
