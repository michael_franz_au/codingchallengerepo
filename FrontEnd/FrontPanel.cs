﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using RestSharp.Authenticators;
using VendingModel;
using System.Text.Json;
using System.Text.Json.Serialization;
using VendingMachine;

namespace VendingMachine
{
    public partial class FrontPanel : Form
    {
        int? ProductVendingId;
        string sProductVendingId;
        VendingProduct CurrentSelection;

        RestClient client = new RestClient("https://localhost:44317/");

        public FrontPanel()
        {
            InitializeComponent();
            ProductVendingId = null;
            sProductVendingId = "";
            CurrentSelection = new VendingProduct();

        }

        private void AddMoney(decimal amt)
        {
            var request = new RestRequest("Operations/AddMoney/", DataFormat.Json);
            request.AddJsonBody(new VendingMoney() { amount = amt });
            var response = client.Post(request);
            UpdateDisplay(response);
        }

        private void UpdateProductKey(int key)
        {
            if (sProductVendingId.Length == 2) sProductVendingId = sProductVendingId.Substring(1) + key.ToString();
            if (sProductVendingId.Length == 1) sProductVendingId = sProductVendingId + key.ToString();
            if (sProductVendingId.Length == 0) sProductVendingId = key.ToString();
            txtID.Text = sProductVendingId;
        }

        private void QueryProduct(int key)
        {
            UpdateProductKey(key);

            if (sProductVendingId.Length > 0)
            {
                var request = new RestRequest($"Operations/QueryProduct/{sProductVendingId}");
                var response = client.Get(request);
                SelectedProduct(response);
            }
        }

 
        private void UpdateDisplay(IRestResponse response)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                OperationResult result = JsonSerializer.Deserialize<OperationResult>(response.Content, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                txtBalance.Text = (result.balance).ToString();

                if (result.products.Where(x => x.isPurchased == false) == null)
                {
                    txtAdmin.Text = $"Machine has no products";
                }
                else
                {
                    txtAdmin.Text = $"Machine has {result.products.Where(x => x.isPurchased == false).Count() } products";
                }
                    txtMessage.Text = result.display_message;
                if (result.vending_result == VendingResult.Refund)
                {
                    txtMoneyReturn.Text = (result?.refund ?? 0).ToString();
                }
                else
                {
                    txtMoneyReturn.Clear();
                }

            }

        }
        private void SelectedProduct(IRestResponse response)
        {
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                OperationResult result = JsonSerializer.Deserialize<OperationResult>(response.Content, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                if (result.vending_result == VendingResult.ProductsAvailableInLane)
                {
                    CurrentSelection = result.product;
                    txtProduct.Text = CurrentSelection.Name;
                    txtPrice.Text = (CurrentSelection.Price).ToString();

                }

            }

        }
        private void bntRefund_Click(object sender, EventArgs e)
        {
            var request = new RestRequest("Operations/Refund/", DataFormat.Json);
            var response = client.Get(request);
            UpdateDisplay(response);
        }

        private void key_0_Click(object sender, EventArgs e)
        {
            QueryProduct(0);
        }

        private void key_1_Click(object sender, EventArgs e)
        {
            QueryProduct(1);
        }

        private void key_2_Click(object sender, EventArgs e)
        {
            QueryProduct(2);
        }

        private void key_3_Click(object sender, EventArgs e)
        {
            QueryProduct(3);
        }

        private void key_4_Click(object sender, EventArgs e)
        {
            QueryProduct(4);
        }

        private void key_5_Click(object sender, EventArgs e)
        {
            QueryProduct(5);
        }

        private void key_6_Click(object sender, EventArgs e)
        {
            QueryProduct(6);
        }

        private void key_7_Click(object sender, EventArgs e)
        {
            QueryProduct(7);
        }

        private void key_8_Click(object sender, EventArgs e)
        {
            QueryProduct(8);
        }

        private void key_9_Click(object sender, EventArgs e)
        {
            QueryProduct(9);

        }

        private void btnTwoDollar_Click(object sender, EventArgs e)
        {
            AddMoney(200);
        }

        private void bntOneDollar_Click(object sender, EventArgs e)
        {
            AddMoney(100);
        }

        private void btnFiftyCents_Click(object sender, EventArgs e)
        {
            AddMoney(50);
        }

        private void bntTwentyCents_Click(object sender, EventArgs e)
        {
            AddMoney(20);
        }

        private void bntTenCents_Click(object sender, EventArgs e)
        {
            AddMoney(10);
        }

        private void bntFiveCents_Click(object sender, EventArgs e)
        {
            AddMoney(5);
        }

        private void txtBalance_TextChanged(object sender, EventArgs e)
        {
            txtBalance.Text = string.Format("{0:#,##0}", double.Parse(txtBalance.Text));
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            var success = int.TryParse(txtProduct.Text, out int result);
            if (success)
            {
                ProductVendingId = result;
            }

        }

        private void ClearVendingScreen()
        {
            txtID.Text = "";
            txtPrice.Text = "";
            txtProduct.Text = "";
            sProductVendingId = "";
        }

        private void GridConfiguration()
        {

            this.dgvVendingChute.Columns["Key"].Visible = false;
            this.dgvVendingChute.Columns["isPurchased"].Visible = false;
            this.dgvVendingChute.Columns["Price"].Visible = false;
        }
        private void bntClear_Click(object sender, EventArgs e)
        {
            ClearVendingScreen();
        }

        private void bntFill_Click(object sender, EventArgs e)
        {
            var request = new RestRequest($"Operations/FillMachine", DataFormat.Json);
            request.AddJsonBody(null);
            var response = client.Put(request);
            UpdateDisplay(response);
        }

        private void btnEmpty_Click(object sender, EventArgs e)
        {
            var request = new RestRequest($"Operations/EmptyMachine", DataFormat.Json);
            request.AddJsonBody(null);
            var response = client.Delete(request);
            UpdateDisplay(response);
        }

        private void btnVend_Click(object sender, EventArgs e)
        {
            var request = new RestRequest($"Operations/Purchase", DataFormat.Json);
            request.AddJsonBody(CurrentSelection);
            var response = client.Put(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                OperationResult result = JsonSerializer.Deserialize<OperationResult>(response.Content, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                if (result.vending_result == VendingResult.ProductsVendSuccess)
                {
                    dgvVendingChute.DataSource = bsChute;
                    bsChute.Add(result.product);
                    ClearVendingScreen();
                    GridConfiguration();
                }
            }
            UpdateDisplay(response);
       
        }

        private void bntEmptyChute_Click(object sender, EventArgs e)
        {
            bsChute.Clear();
            dgvVendingChute.DataSource = bsChute;
            txtMessage.Text = "Chute Emptied";

            GridConfiguration();

        }

        private void btnRefundedMoney_Click(object sender, EventArgs e)
        {
            txtMoneyReturn.Clear();
        }
    }
}
