﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingModel
{
    public class VendingMoney
    {
        public VendingMoney()
        {
            transaction_id = new Guid();
        }

        public decimal amount { get; set; }

        public Guid transaction_id { get; set; }

    }
    public class VendingProduct
    {
        public VendingProduct()
        {

        }

        public int Key { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }

        public bool isPurchased { get; set; }
    }
    
    public enum VendingResult
    {
        None = 0,
        AddedMoney = 1,
        AddMoneyFailed = 2,
        Refund = 3,
        RefundFailed = 4,
        InsufficientFunds = 5,
        NoProductsInLane = 6,
        ProductsAvailableInLane = 7,
        ProductsVendSuccess = 8,
    }

    public class OperationResult
    {

        public OperationResult()
        {
 
        }
        public VendingResult vending_result { get; set; }

        public decimal refund { get; set; }

        public decimal balance { get; set; }

        public string display_message { get; set; }

        /// <summary>
        /// A Selected or vended product
        /// </summary>
        public VendingProduct product { get; set; }

        /// <summary>
        /// Unsold products
        /// </summary>
        public List<VendingProduct> products { get; set; }
    }
}
